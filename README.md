This is a Apache Thrift without boost pointers
==============================================

tl;dr
-----

See commit history in https://bitbucket.org/macrodata/thrift-no-boost-ptr

About
-----

Compiled binaries located under /usr/local/thrift

To install, run container by

```
docker run --rm -v YOUR-INSTALL-PATH:/usr/local/thrift macrodata/thrift-no-boost-ptr
```

